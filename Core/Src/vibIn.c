/*
 * vibIn.c
 *
 *  Created on: Aug 5, 2020
 *      Author: GeorgeBlat
 */


#include "main.h"

//TaskHandle_t vibInHandle;

void vibInThread(void* param)
{
	Xmitters event = phone;

	for (;;)
	{
		ulTaskNotifyTake(true, portMAX_DELAY);
		for (int i = 0; i < 10; i++, vTaskDelay(15))
		{
			if ((Vib_In_GPIO_Port->IDR & Vib_In_Pin) == 0)
			{
				goto label;
			}
		}
		xQueueSend(queue418, &event, portMAX_DELAY);

		label:;
	}
}


void EXTI_IRQHandlerGPIO_PIN_7(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(Vib_In_Pin) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(Vib_In_Pin);
		markActive();

		BaseType_t	higherPriorityWoken = false;
		vTaskNotifyGiveFromISR(vibInHandle, &higherPriorityWoken);
		portYIELD_FROM_ISR( higherPriorityWoken );

		HAL_PWR_DisableSleepOnExit();
	}
}
