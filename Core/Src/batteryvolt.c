/*
 * batteryvolt.c
 *
 *  Created on: Jul 22, 2020
 *      Author: GeorgeBlat
 */


#include "main.h"
#include "lptim.h"

extern ADC_HandleTypeDef hadc;

#define MINIMUMVOLT		2300
#define CTSPERSEC		(37000/32)
#define Period			(CTSPERSEC * 10)
#define Pulse			(Period - CTSPERSEC / 10)

bool VoltageIsLow	= false;


void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
	U32 ADCReading;
	U16 VREFINT_CAL =	*(U16*)0x1FF80078;

	if (!VoltageIsLow && hrtc->Instance == RTC)
	{
		HAL_ADCEx_EnableVREFINT();
		ADC->CCR |= ADC_CCR_VREFEN;
		HAL_ADC_Start(&hadc);
		HAL_ADC_PollForConversion(&hadc, 2);
		ADCReading = HAL_ADC_GetValue(&hadc);
		U32 Vccmv = MulDiv(3000, VREFINT_CAL, ADCReading);
		HAL_ADC_Stop(&hadc);

		// disable VREFINT
		ADC->CCR &= ~ADC_CCR_VREFEN;
		HAL_ADCEx_DisableVREFINT();

		if (Vccmv < MINIMUMVOLT)
		{
			HAL_RTC_DeactivateAlarm(hrtc, RTC_ALARM_A);
			HAL_RTC_DeInit(hrtc);
			VoltageIsLow = true;
			MX_LPTIM1_Init();
			HAL_LPTIM_PWM_Start_IT(&hlptim1, Period, Pulse);
		}
	}
}

void HAL_LPTIM_CompareMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
	if (hlptim->Instance == LPTIM1)
	{
		ledyellow(true);
	}
}

void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
	if (hlptim->Instance == LPTIM1)
	{
		ledyellow(false);
	}
}

