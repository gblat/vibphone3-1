/*
 * pulses.c++
 *
 *  Created on: Jul 13, 2020
 *      Author: GeorgeBlat
 */

#include "main.h"
#include <string.h>

TaskHandle_t listnHandle;
QueueHandle_t queuePulse;

typedef enum {rfRaising, rfFalling} rf_t;

void setRisingOrFalling(U32 pin, rf_t rf)
{
	switch(rf)
	{
	case rfRaising:
		EXTI->FTSR &= ~pin;
		EXTI->RTSR |= pin;
		break;

	case rfFalling:
		EXTI->RTSR &= ~pin;
		EXTI->FTSR |= pin;
		break;
	}
}

struct listnEvent_t
{
	U32		tickCnt;
	rf_t	rf;
};

class Pulse;

U32 nPulses;
Pulse *pulses;


class Pulse
{
	inline void	raisingPulse(TickType_t time);
	void	fallingPulse(TickType_t time);
	void	evaluateNSend(U32 totalBurstTime);
	void* 	operator new(size_t size)
	{
		void* p = pvPortMalloc(size);
		memset(p, 0, size);		// clear the object data
		return p;
	}

	void	clear(void)
	{
		pulseStart =
		pulseEnd =
		timeBefore =
		pulseTime = 0;
	}

	U32 pulseStart;
	U32 pulseEnd;
	U32 timeBefore;
	U16 pulseTime;

	friend void listnThread(void *param);
	friend U32 totalOnTime(void);
	friend void pulsesClear(void);
};

Pulse thisPulse;

U32 totalOnTime(void)
{
	U32 result = 0;
	for (U32 i = 0; i < nPulses; i++)
	{
		result += pulses[i].pulseTime;
	}
	return result;
}


void Pulse::raisingPulse(TickType_t time)
{
	clear();

	pulseStart = time;
	if (nPulses >= 1)
	{
		timeBefore = pulseStart - pulses[nPulses - 1].pulseEnd;
		if (timeBefore > 900UL)
		{
			nPulses = 0;
		}
	}
}

void Pulse::fallingPulse(TickType_t time)
{
	U32 totalBurstTime;

	//	ledyellow(false);

	pulseEnd = time;
	pulseTime = pulseEnd - pulseStart;
	if (checkBetween(pulseTime, 100, 30) ||  checkBetween(pulseTime, 500, 30))
	{
		pulses[nPulses++] = thisPulse;

		if (nPulses >= 3)
		{
			totalBurstTime = pulseEnd - pulses[0].pulseStart;

			if ((nPulses == 3  && totalBurstTime >= 2000) || nPulses == 4)
			{
				evaluateNSend(totalBurstTime);
			}
			else if (nPulses > 4)
			{
				pulsesClear();
			}
		}
	}
}

void Pulse::evaluateNSend(U32 totalBurstTime)
{
	U32 onTime = totalOnTime();
	Xmitters event;

	if (nPulses == 3 && checkBetween(onTime, 1500, 30) && checkBetween(totalBurstTime, 2500, 30))
	{
		// accept as T3
		event = fire;
	}
	else if (nPulses == 4 && checkBetween(onTime, 400, 30) && checkBetween(totalBurstTime, 700, 30))
	{
		// accept as T4
		event = CO;
	}
	else
	{
		return;
	}
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	xQueueSendFromISR(queue418, &event, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	pulsesClear();
}

void listnThread(void *param)
{
	listnEvent_t listnEvent;

	EXTI->IMR |= T3T4_IN_Pin;	// enable EXTI interrupt
	for (;;)
	{
		xQueueReceive(queuePulse, &listnEvent, portMAX_DELAY);
		if (listnEvent.rf == rfRaising)
		{
			thisPulse.raisingPulse(listnEvent.tickCnt);
		}
		else
		{
			thisPulse.fallingPulse(listnEvent.tickCnt);
		}
	}
}

void EXTI_IRQHandlerGPIO_PIN_7(void)
{
	listnEvent_t listnEvent;


	/* EXTI line interrupt detected */
	if(__HAL_GPIO_EXTI_GET_IT(T3T4_IN_Pin) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(T3T4_IN_Pin);
		markActive();
		BaseType_t	higherPriorityWoken = false;
		listnEvent.rf = (T3T4_IN_GPIO_Port->IDR & T3T4_IN_Pin) ? rfRaising : rfFalling;
		listnEvent.tickCnt = HAL_GetTick();

		xQueueSendFromISR(queuePulse, &listnEvent, &higherPriorityWoken);
		portYIELD_FROM_ISR( higherPriorityWoken );
		HAL_PWR_DisableSleepOnExit();
	}
}

void pulsesClear(void)
{
	nPulses = 0;
	thisPulse.clear();
	xQueueReset(queuePulse);
//	xQueueReset(queue418);
	for (int i = 0; i < 6; i++)
	{
		pulses[i].clear();
	}
}

void pulsesInit(void)
{
	pulses = new Pulse[6];
	queuePulse = xQueueCreate(6, sizeof(listnEvent_t));
	createTask(listn, 100, 3);
}
