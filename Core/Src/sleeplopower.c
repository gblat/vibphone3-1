/*
 * sleeplopower.c
 *
 *  Created on: May 19, 2019
 *      Author: George Blat
 */

#include "main.h"



volatile U32		activeTicks;
U16* VREFINT_CAL =	(U16*)0x1FF80078;
volatile	bool	workInProcess;

batteryState_t batteryState = batteryOK, priorState = batteryOK;

static inline U32 ticksPerMsec(U32 millisec)
{
	return MulDiv(37UL, millisec, 128UL);
}

//#define INDEBUG
void vApplicationIdleHook( void )
{
	static bool inSleep = false;
	if (!inSleep && hasExpired(activeTicks, 2000))
	{


		inSleep = true;
		// stop systick

		ledyellow(false);

		RCC->IOPSMENR &= ~0b10011110;
		RCC->IOPSMENR |= RCC_IOPSMENR_GPIOASMEN;		// enable PORT A only

		RCC->AHBSMENR &= ~(BIT24|BIT12|BIT8|BIT0);
		RCC->APB2SMENR &= ~(BIT22|BIT14|BIT12|BIT9|BIT5|BIT2|BIT0);
		RCC->APB2SMENR |= RCC_APB2SMENR_DBGSMEN;		// allow debug

		RCC->APB1SMENR &=
				~(BIT31|BIT30|BIT29|BIT28|BIT22|BIT21|BIT20|BIT19|
						BIT18|BIT17|BIT14|BIT11|BIT5|BIT4|BIT1|BIT0);
//		RCC->APB1SMENR |= RCC_APB1SMENR_LPTIM1SMEN;
		FLASH->ACR |= FLASH_ACR_SLEEP_PD;

		// disable VREFINT
		HAL_ADCEx_DisableVREFINT();

//		U32 primask = __get_PRIMASK();
//		__set_PRIMASK(primask | BIT0);

		workInProcess = false;

		// lower clock freq
	    MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, RCC_CFGR_HPRE_DIV64);

	    // have interrupts wake up as if event
//	    SCB->SCR |= SCB_SCR_SEVONPEND_Msk;

		HAL_SuspendTick();

		HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);

	    MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, RCC_CFGR_HPRE_DIV1);

	    // restore systick
		HAL_ResumeTick();

//	    SCB->SCR &= ~SCB_SCR_SEVONPEND_Msk;

//	    primask = __get_PRIMASK();
//		__set_PRIMASK(primask & ~BIT0);
		inSleep = false;
		markActive();
		ledyellow(true);
	}
}

void markActive(void)
{
	setInitial(&activeTicks);
}


void conditionalSleepOnExit(void)
{
	if (workInProcess)
		HAL_PWR_DisableSleepOnExit();
	else
		HAL_PWR_EnableSleepOnExit();
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	BaseType_t	higherPriorityWoken = false;

	if (GPIO_Pin == TestButton_Pin)
	{
		static U32	timeVar;
		U8			event;
		bool 		pressed = (TestButton_GPIO_Port->IDR & TestButton_Pin) == 0;


		markActive();
		if (pressed)
		{
			setInitial(&timeVar);
		}
		else	// released
		{
			U32 lapsed = getTicksLapsed(timeVar, NULL);

			if (lapsed > 2)
			{
				if (lapsed < 1000)
				{
					// send fire code
					event = fire;
				}
				else
				{
					// send CO code
					event = CO;
				}
				xQueueSendFromISR(queue418, &event, &higherPriorityWoken);
				portYIELD_FROM_ISR( higherPriorityWoken );
			}
		}
	}
}

#define MASK	0b111

void HAL_IncTick(void)
{
	static	U32	skipCount = 0;
	static	bool priorState = false;
	static	U8	sequence = 0;

	uwTick++;

	if (++skipCount < 5)
		return;

	skipCount = 0;

	BaseType_t	higherPriorityWoken = false;
	bool	InValue = (T3T4_IN_GPIO_Port->IDR & T3T4_IN_Pin) != 0;

	sequence <<= 1;
	if (InValue)
	{
		sequence |= BIT0;
	}

	sequence &= MASK;

	if (sequence == 0 || sequence == MASK)
	{
		if (InValue != priorState)
		{
			if (InValue)
				ledyellow(true);
			markActive();
			workInProcess = true;
			priorState = InValue;
			dbgScope(InValue);
			xTaskNotifyFromISR(soundHandle, (U32)InValue, eSetValueWithOverwrite, &higherPriorityWoken);
			portYIELD_FROM_ISR( higherPriorityWoken );
		}
	}

}

extern ADC_HandleTypeDef hadc;

U16 getVDDmv(void)
{
	U16	result;

	// enable the clock at the beginning
	RCC->APB2SMENR |= RCC_APB2SMENR_ADCSMEN;

	// Wake-up the VREFINT
	ADC->CCR |= ADC_CCR_VREFEN; /* (5) */
	HAL_ADC_Start(&hadc);
	HAL_ADC_PollForConversion(&hadc, ULONG_MAX);
	result =  MulDiv(3000, *VREFINT_CAL, HAL_ADC_GetValue(&hadc));
	// disable VREFINT
	ADC->CCR &= ~ADC_CCR_VREFEN;
	// disable the clock to save power
	RCC->APB2SMENR &= ~RCC_APB2SMENR_ADCSMEN;
	return result;
}


HAL_StatusTypeDef setupLPTimer(batteryState_t newState)
{
	HAL_StatusTypeDef result;

	result = HAL_LPTIM_PWM_Stop_IT(&hlptim1);

	if (newState == batteryLow)
	{
		HAL_LPTIM_PWM_Start_IT(&hlptim1, ticksPerMsec(10000), ticksPerMsec(100));
	}
	else	// battery OK. First setup
	{
		HAL_LPTIM_PWM_Start_IT(&hlptim1, ticksPerMsec(180000), 0xfffe);
	}
	return result;
}

void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
	static	U32	count = 0;
	U16		batVoltage;
	UNUSED(hlptim);

	switch (batteryState)
	{
	case batteryOK:
		if (++count < 480)	// once a day 480 * 180
			break;
		count = 0;
		// must test battery
		batVoltage = getVDDmv();
		batteryState = (batVoltage < 2200) ? batteryLow : batteryOK;
		if (batteryState != priorState)
		{
			priorState = batteryState;
			setupLPTimer(batteryState);
		}
		break;

	case batteryLow:
		// turn on LED
		GPIOA->BRR = LED2_Pin;
		break;
	}

	conditionalSleepOnExit();
}

void HAL_LPTIM_CompareMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
	UNUSED(hlptim);
	// turn off LED
	GPIOA->BSRR = LED2_Pin;

	conditionalSleepOnExit();
}

void sleeplopowerInit(void)
{
	U16 mVolts = getVDDmv();
	batteryState = (mVolts < 2200) ? batteryLow : batteryOK;
//	setupLPTimer(batteryState);

}

