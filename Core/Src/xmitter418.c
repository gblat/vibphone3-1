/*
 * xmitter418.c
 *
 *  Created on: May 15, 2019
 *      Author: George Blat
 */

#include "main.h"
#include "stm32l0xx_hal_tim.h"
#include "tim.h"
#include "queue.h"

#define zeroPacket		1028
#define onePacket		3166
#define	period			4020
#define	bitsPerPacket	28

volatile	enum{invalid, setup, run} PWM_State = invalid;
volatile	U32	vMedcode;

HAL_StatusTypeDef TIM_PWM_Start_IT(TIM_HandleTypeDef *htim, uint32_t Channel)
{
	// missing in MXCube
	 __HAL_TIM_ENABLE_IT(htim, TIM_IT_UPDATE);
	return HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_1);

}

HAL_StatusTypeDef TIM_PWM_Stop_IT(TIM_HandleTypeDef *htim, uint32_t Channel)
{
	HAL_StatusTypeDef result = HAL_TIM_PWM_Stop_IT(htim, Channel);
	// missing in MXCube
	__HAL_TIM_DISABLE_IT(htim, TIM_IT_UPDATE);

	return result;
}

void EndOfPeriod(void)
{
	RF_ON_GPIO_Port->BSRR = RF_ON_Pin;		// turn-on RF
}

void EndOfPulse(void)
{
	RF_ON_GPIO_Port->BRR = RF_ON_Pin;		// turn-off RF

	static	U32 msg;
	static	U32 count;

	if (PWM_State != invalid)
	{
		if (PWM_State == setup)
		{
			msg = vMedcode;
			count = bitsPerPacket;
			PWM_State = run;
		}

		if (count > 0)
		{
			count--;
			TIM2->CCR1 = (msg & 1) ? onePacket : zeroPacket;
			msg >>= 1;
//			RF_ON_GPIO_Port->BRR = RF_ON_Pin;	// turn-off RF
		}
		else
		{
			TIM_PWM_Stop_IT(&htim2, TIM_CHANNEL_1);
			PWM_State = invalid;
		}
	}
}

void sendPackets(U32 medcode, U32 times)
{
	// turn on the red led
	ledred(true);
	TickType_t timevar = xTaskGetTickCount();
	while(times--)
	{
		markActive();
		vMedcode = medcode;
		PWM_State = setup;

		TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_1);
		HAL_TIM_GenerateEvent(&htim2, TIM_EVENTSOURCE_CC1);

		// wait until the packet is out
		vTaskDelayUntil(&timevar, 105);
	}
	// turn off the red led
	ledred(false);
}

void xmitter418Thread(void* parm)
{
	UNUSED(parm);
	medallionCode_t	medallionCode;
	static Xmitters	priorEvent = 0;
	Xmitters	newEvent;

	// at the beginning blink the yellow light twice
	for (int i = 0; i < 3; i++)
	{
		vTaskDelay(75);
		ledyellow(true);
		vTaskDelay(50);
		ledyellow(false);
	}

	// get address from eeprom
	medallionCode.address = *(U32*)DATA_EEPROM_BASE;
	medallionCode.supervisor = 0;

	while (true)
	{
		// wait for T3 or T4 sounds
		xQueueReceive(queue418, &newEvent,portMAX_DELAY);

		if (newEvent == fire || newEvent == CO)
		{
			medallionCode.eventType = newEvent & 7;
			if (newEvent !=  priorEvent)
			{
				priorEvent = newEvent;
				sendPackets(medallionCode.medCode, 28);
			}
			else
			{
				sendPackets(medallionCode.medCode, 12);
			}
		}
	}
}

void MY_TIM2_IRQHandler(void)
{
	if ((__HAL_TIM_GET_FLAG(&htim2, TIM_FLAG_CC1) != RESET)
			&& (__HAL_TIM_GET_IT_SOURCE(&htim2, TIM_IT_CC1) != RESET))
	{
		__HAL_TIM_CLEAR_IT(&htim2, TIM_IT_CC1);
		EndOfPulse();
	}
	else if ((__HAL_TIM_GET_FLAG(&htim2, TIM_FLAG_UPDATE) != RESET)
			&& (__HAL_TIM_GET_IT_SOURCE(&htim2, TIM_IT_UPDATE) != RESET))
	{
		__HAL_TIM_CLEAR_IT(&htim2, TIM_IT_UPDATE);
		EndOfPeriod();
	}
}
