/*
 * stoplowpower.c
 *
 *  Created on: Jul 10, 2019
 *      Author: George Blat
 */

#include "main.h"
#include "task.h"


volatile U32		activeTicks;
U16* VREFINT_CAL =	(U16*)0x1FF80078;
volatile	bool	workInProcess;

batteryState_t batteryState = batteryOK, priorState = batteryOK;


void vApplicationIdleHook( void )
{
	if (hasExpired(activeTicks, 1000))
	{
		HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);

	}
}


void EXTI_IRQHandlerGPIO_PIN_6(void)
{
	static U32	timeVar;
	U32			event;
	bool 		pressed = (TestButton_GPIO_Port->IDR & TestButton_Pin) == 0;

	/* EXTI line interrupt detected */
	if(__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_6) != RESET)
	{
		__HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_6);


		markActive();
		if (pressed)
		{
			setInitial(&timeVar);
		}
		else	// released
		{
			U32 lapsed = getTicksLapsed(timeVar, NULL);

			if (lapsed > 2)
			{
				BaseType_t	higherPriorityWoken = false;
				event = phone;
				xQueueSendFromISR(queue418, &event, &higherPriorityWoken);
				portYIELD_FROM_ISR( higherPriorityWoken );
			}
		}
		HAL_PWR_DisableSleepOnExit();
	}
}
