/*
 * t3t4.h
 *
 *  Created on: May 15, 2019
 *      Author: George Blat
 */

#ifndef INC_VIB_H_
#define INC_VIB_H_

#ifdef __cplusplus
extern "C" {
#else
typedef uint8_t	bool;
#endif


#include "cmsis_os.h"
#include "FreeRTOS.h"


//unsigned
typedef uint8_t		U8;
typedef uint16_t 	U16;
typedef uint32_t	U32;
typedef uint64_t	U64;

//signed
typedef int8_t		S8;
typedef int16_t		S16;
typedef int32_t		S32;
typedef int64_t		S64;

typedef void *PVOID;

#define FALSE	0
#define TRUE	(!FALSE)
#define false	FALSE
#define true	TRUE

#define ERROR_	(-1l)

#define _BV(n)	(1UL << n)

#define BIT0	_BV(0)
#define BIT1	_BV(1)
#define BIT2	_BV(2)
#define BIT3	_BV(3)
#define BIT4	_BV(4)
#define BIT5	_BV(5)
#define BIT6	_BV(6)
#define BIT7	_BV(7)
#define BIT8	_BV(8)
#define BIT9	_BV(9)
#define BIT10	_BV(10)
#define BIT11	_BV(11)
#define BIT12	_BV(12)
#define BIT13	_BV(13)
#define BIT14	_BV(14)
#define BIT15	_BV(15)
#define	BIT16	_BV(16)
#define	BIT17	_BV(17)
#define	BIT18	_BV(18)
#define	BIT19	_BV(19)
#define	BIT20	_BV(20)
#define	BIT21	_BV(21)
#define	BIT22	_BV(22)
#define	BIT23	_BV(23)
#define	BIT24	_BV(24)
#define	BIT25	_BV(25)
#define	BIT26	_BV(26)
#define	BIT27	_BV(27)
#define	BIT28	_BV(28)
#define	BIT29	_BV(29)
#define	BIT30	_BV(30)
#define	BIT31	_BV(31)

#define INVALID	(-1L)

#define	ULONG_MAX	0xffffffffUL

#define max(a, b)			\
({							\
	typeof(a) _a = (a);		\
	typeof(b) _b = (b);		\
	(_a > _b) ? _a : _b;	\
})

#define min(a, b)			\
({							\
	typeof(a) _a = (a);		\
	typeof(b) _b = (b);		\
	(_a < _b) ? _a : _b;	\
})

#define abs(a) 				\
({							\
	typeof(a) _a = a;		\
	(_a < 0) ? -_a : _a;	\
})

static inline bool inBetween(U32 value, U32 min, U32 max)
{
	return (value >= min) && (value <= max);
}

static inline bool checkBetween(U32 value, U32 expect, U32 percent)
{
	U32 delta = expect * percent / 100UL;

	return inBetween(value, expect - delta, expect + delta);
}

static inline void setInitial(volatile U32 *timeVar)
{
	*timeVar = xTaskGetTickCount();
}


static inline U32 getTicksLapsed(U32 lastCount, U32 *newCount)
{
	U32 newTickCount = xTaskGetTickCount();
	U32 result = newTickCount - lastCount;

	if (newCount != NULL)
	{
		*newCount = newTickCount;
	}

	return result;
}

typedef enum {batteryOK, batteryLow} batteryState_t;


U16 getVDDmv(void);
HAL_StatusTypeDef setupLPTimer(batteryState_t newState);
void sleeplopowerInit(void);
extern batteryState_t priorState, batteryState;
extern volatile	bool	workInProcess;
void PulsesIncTime(void);
void EXTI_IRQHandlerGPIO_PIN_6(void);
void EXTI_IRQHandlerGPIO_PIN_7(void);
void pulsesInit(void);
void listnThread(void *param);
void pulsesClear(void);


typedef enum
{
	info, door = 1, weather, sound, fire, phone, CO
} Xmitters;


typedef union
{
	struct
	{
		U32 address		:20;
		U32 supervisor	:4;
		U32 			:1;
		U32 eventType	:3;

	};
	U32 medCode;
} medallionCode_t;

extern volatile	medallionCode_t	medallionCode;
extern TaskHandle_t	xmitter418Handle;
extern TaskHandle_t soundHandle;
extern TaskHandle_t listnHandle;
extern TaskHandle_t vibInHandle;
extern volatile U32 activeTicks;
extern QueueHandle_t queue418;

static inline bool hasExpired(U32 timeVar, U32 ticks)
{
	U32	temp = getTicksLapsed(timeVar, NULL);

	return temp >= ticks;
}

static inline U64 MulDiv(U32 N1, U32 N2, U32 D)
{
	U64 result = (U64)N1 * (U64)N2 + D / 2UL;
	result /= D;
	return result;
}

static inline void markActive(void)
{
	setInitial(&activeTicks);
}


#define createTask(name, stacksize, priority)	\
	xTaskCreate(	\
		name##Thread, #name, stacksize, NULL,  \
		priority, &name##Handle)

#define declareTask(name)			\
	void name##Thread(void*);		\
	TaskHandle_t name##Handle



#ifdef __cplusplus
}
#endif


#endif /* INC_VIB_H_ */
