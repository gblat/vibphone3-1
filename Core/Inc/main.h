/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <vib.h>

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RF_ON_Pin GPIO_PIN_0
#define RF_ON_GPIO_Port GPIOA
#define dbgout_Pin GPIO_PIN_4
#define dbgout_GPIO_Port GPIOA
#define TestButton_Pin GPIO_PIN_6
#define TestButton_GPIO_Port GPIOA
#define TestButton_EXTI_IRQn EXTI4_15_IRQn
#define Vib_In_Pin GPIO_PIN_7
#define Vib_In_GPIO_Port GPIOA
#define Vib_In_EXTI_IRQn EXTI4_15_IRQn
#define LED_RED_Pin GPIO_PIN_9
#define LED_RED_GPIO_Port GPIOA
#define LED_YELLOW_Pin GPIO_PIN_10
#define LED_YELLOW_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

static inline void ledyellow(bool onoff)
{
	LED_YELLOW_GPIO_Port->BSRR = (onoff) ? (LED_YELLOW_Pin << 0x10) : LED_YELLOW_Pin ;
}

static inline void ledred(bool onoff)
{
	LED_RED_GPIO_Port->BSRR = (onoff) ? (LED_RED_Pin << 0x10) : LED_RED_Pin ;
}


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
